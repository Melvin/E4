<?php
class Database{

    private $pdo;
	
	/**
     * Database constructor.
     * @param $param_user string
     * @param $param_mdp string
     * @param $database_name string	 
     * @param $param_host string	  
     */	
	public function __construct($param_user, $param_mdp, $database_name, $param_host = 'db723051449.db.1and1.com'){
        $this->pdo = new PDO("mysql:dbname=$database_name;host=$param_host", $param_user, $param_mdp);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    /**
	 * Permet l'éxécution d'une requête préparer 
     * @param $query
     * @param bool|array $params
     * @return PDOStatement
     */
    public function query($query, $params = false){
        if($params){
            $req = $this->pdo->prepare($query);
            $req->execute($params);
        }else{
            $req = $this->pdo->query($query);
        }
        return $req;
    }

    /**
     * Permet de récupérer le dernier id insérer dans la base de donnée
     * @return id
     */		
    public function lastInsertId(){
        return $this->pdo->lastInsertId();
    }

}