﻿<?php
class Auth{

    private $options = [
        'restriction_msg' => "Vous n'avez pas le droit d'accéder à cette page"
    ];
    private $session;

	/**
     * Auth constructor.
     * @param $session string
     * @param $option string
     */
    public function __construct($session, $options = []){
        $this->options = array_merge($this->options, $options);
        $this->session = $session;
    }

	/**
     * Permet de réaliser le cryptage du mot de passe en le hachant
     * @param $password
     * @return string Le mot de passe crypter
     */
    public function hashPassword($password){
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * Permet d'inscrire l'utilisateur, de supprimer l'utilisateur de la table attente et d'envoyer un email de confirmation
     * @param $db
     * @param $name	 
     * @param $firstname	 
     * @param $pays
     * @param $address
     * @param $postalcode
     * @param $city
     * @param $tel	
     * @param $email
     * @param $civility
     * @param $pseudo
     * @param $password	 
     */
    public function register($db, $name, $firstname, $pays, $address, $postalcode, $city, $tel, $email, $civility, $pseudo, $password){
        $password = $this->hashPassword($password);
        $token = Str::random(60);
        $db->query("INSERT INTO UTILISATEURS SET NOM = ?, PRENOM = ?, PAYS = ?, ADRESSE = ?, CODE_POSTAL = ?, VILLE = ?, TELEPHONE = ?, EMAIL = ?, CIVILITE = ?, PSEUDO = ?, MOT_DE_PASSE = ?, CONFIRM_TOKEN = ?", [
            $name, 
			$firstname,
            $pays,			
			$address, 
			$postalcode, 
			$city, 
			$tel, 
			$email, 
			$civility, 
			$pseudo, 
			$password,
            $token
        ]);
        $user_id = $db->lastInsertId();
		$db->query('DELETE FROM ATTENTES');
        mail($email, 'Confirmation de votre compte', "Afin de valider votre compte merci de cliquer sur ce lien\n\nhttps://www.japan-zone.fr/confirm?id=$user_id&token=$token", "From: Japan Zone <japzone72@gmail.com>\r\n");
    }

	/**
     * Permet d'inscrire l'utilisateur l'utilisateur en table attente, à fin de valider le payement
     * @param $db
     * @param $name	 
     * @param $firstname	 
     * @param $pays
     * @param $address
     * @param $postalcode
     * @param $city
     * @param $tel	
     * @param $email
     * @param $civility
     * @param $pseudo
     * @param $password	 
     */
    public function attentes($db, $name, $firstname, $pays, $address, $postalcode, $city, $tel, $email, $civility, $pseudo, $password){
        $db->query("INSERT INTO ATTENTES SET NOM = ?, PRENOM = ?, PAYS = ?, ADRESSE = ?, CODE_POSTAL = ?, VILLE = ?, TELEPHONE = ?, EMAIL = ?, CIVILITE = ?, PSEUDO = ?, MOT_DE_PASSE = ?", [
            $name, 
			$firstname,
            $pays,			
			$address, 
			$postalcode, 
			$city, 
			$tel, 
			$email, 
			$civility, 
			$pseudo, 
			$password
        ]);

    }	

	/**
     * Permet de confirmer le compte à fin de pouvoir utiliser son compte
     * @param $db
     * @param $user_id	 
     * @param $token	 
     * @return bool	 
     */
    public function confirm($db, $user_id, $token){
        $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch();
        if($user && $user->CONFIRM_TOKEN == $token ){
            $db->query('UPDATE UTILISATEURS SET CONFIRM_TOKEN = NULL, CONFIRMER_DATE = NOW() WHERE NUM_UTILISATEURS = ?', [$user_id]);
            $this->session->write('auth', $user);
            return true;
        }
        return false;
    }
	
	/**
     * Permet de resteindre la page qu'à l'utilisateur et non visible pour les visiteurs	 
     */
    public function restrict(){
        if(!$this->session->read('auth')){
            $this->session->setFlash('danger', $this->options['restriction_msg']);
            header('Location: index.php');
            exit();
        }
    }

    /**
     * Permet de lire la session actuel	
     * @return bool ou la session	 
     */
    public function user(){
        if(!$this->session->read('auth')){
            return false;
        }
        return $this->session->read('auth');
    }
	
	/**
     * Permet d'écrire la session d'utilisateur actuel	 
     */
    public function connect($user){
        $this->session->write('auth', $user);
    }	

	/**
     * Permet de modifier le nom et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $name	 
     * @param $user_id	  
     */
    public function updateName($db, $name, $user_id){
        $db->query('UPDATE UTILISATEURS SET NOM = ? WHERE NUM_UTILISATEURS = ?', [$name, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

	/**
     * Permet de modifier le prenom et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $firstname	 
     * @param $user_id	  
     */
    public function updateFirstname($db, $firstname, $user_id){
        $db->query('UPDATE UTILISATEURS SET PRENOM = ? WHERE NUM_UTILISATEURS = ?', [$firstname, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

	/**
     * Permet de modifier la civiliter et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $civility	 
     * @param $user_id	  
     */	
    public function updateCivilite($db, $civility, $user_id){
        $db->query('UPDATE UTILISATEURS SET CIVILITE = ? WHERE NUM_UTILISATEURS = ?', [$civility, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

    /**
     * Permet de modifier le pays et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $pays	 
     * @param $user_id	  
     */	
    public function updatePays($db, $pays, $user_id){
        $db->query('UPDATE UTILISATEURS SET PAYS = ? WHERE NUM_UTILISATEURS = ?', [$pays, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }
	
    /**
     * Permet de modifier l'adresse et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $adresse	 
     * @param $user_id	  
     */
    public function updateAdresse($db, $address, $user_id){
        $db->query('UPDATE UTILISATEURS SET ADRESSE = ? WHERE NUM_UTILISATEURS = ?', [$address, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

    /**
     * Permet de modifier la ville et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $city	 
     * @param $user_id	  
     */	
    public function updateVille($db, $city, $user_id){
        $db->query('UPDATE UTILISATEURS SET VILLE = ? WHERE NUM_UTILISATEURS = ?', [$city, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

    /**
     * Permet de modifier le code postal et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $postalcode	 
     * @param $user_id	  
     */		
    public function updatePostal($db, $postalcode, $user_id){
        $db->query('UPDATE UTILISATEURS SET CODE_POSTAL = ? WHERE NUM_UTILISATEURS = ?', [$postalcode, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

    /**
     * Permet de modifier le telephone et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $tel	 
     * @param $user_id	  
     */		
    public function updateTel($db, $tel, $user_id){
        $db->query('UPDATE UTILISATEURS SET TELEPHONE = ? WHERE NUM_UTILISATEURS = ?', [$tel, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

    /**
     * Permet de modifier l'email et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $email	 
     * @param $user_id	  
     */	
    public function updateEmail($db, $email, $user_id){
        $db->query('UPDATE UTILISATEURS SET EMAIL = ? WHERE NUM_UTILISATEURS = ?', [$email, $user_id]);;
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

    /**
     * Permet de modifier le pseudo et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $pseudo	 
     * @param $user_id	  
     */	
    public function updatePseudo($db, $pseudo, $user_id){
        $db->query('UPDATE UTILISATEURS SET PSEUDO = ? WHERE NUM_UTILISATEURS = ?', [$pseudo, $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }

    /**
     * Permet de modifier l'image de l'avatar et de mettre à jour la base de donnée ainsi que la session d'utilisateur
     * @param $db
     * @param $avatar	 
     * @param $user_id	  
     */	
    public function updateAvatar($db, $avatar, $user_id){
        $db->query('UPDATE UTILISATEURS SET AVATAR = :avatar WHERE NUM_UTILISATEURS = :id', ['avatar' => $avatar, 'id' => $user_id]);
	    $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch(); 
	    $this->connect($user);
    }	

    /**
     * Permet de ce connecter automatiquement avec ce souvenant de l'utilisateur
     * @param $db	  
     */		
    public function connectFromCookie($db){
        if(isset($_COOKIE['remember']) && !$this->user()){
            $remember_token = $_COOKIE['remember'];
            $parts = explode('==', $remember_token);
            $user_id = $parts[0];
            $user = $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ?', [$user_id])->fetch();
            if($user){
                $expected = $user_id . '==' . $user->REMEMBER_ME_TOKEN . sha1($user_id . 'ratonlaveurs');
                if($expected == $remember_token){
                    $this->connect($user);
                    setcookie('remember', $remember_token, time() + 60 * 60 * 24 * 7);
                } else{
                    setcookie('remember', null, -1);
                }
            }else{
                setcookie('remember', null, -1);
            }
        }
    }
	
    /**
     * Permet de connecter l'utilisateur
     * @param $db	 
     * @param $pseudo	 
     * @param $password 
     * @param bool|array $remember	 
	 * @return l'utilisateur
     */	
    public function login($db, $pseudo, $password, $remember = false){
        $user = $db->query('SELECT * FROM UTILISATEURS WHERE (PSEUDO = :pseudo OR EMAIL = :pseudo) AND CONFIRMER_DATE IS NOT NULL', ['pseudo' => $pseudo])->fetch();
        if(password_verify($password, $user->MOT_DE_PASSE)){
            $this->connect($user);
            if($remember){
                $this->remember($db, $user->NUM_UTILISATEURS);
            }
            return $user;
        }else{
            return false;
        }
    }	

    /**
     * Permet de ce souvenir de l'utilisateur en créant un token disponible dans ces données
     * @param $db	 
     * @param $user_id
     */	
    public function remember($db, $user_id){
        $remember_token = Str::random(250);
        $db->query('UPDATE UTILISATEURS SET REMEMBER_ME_TOKEN = ? WHERE NUM_UTILISATEURS = ?', [$remember_token, $user_id]);
        setcookie('remember', $user_id . '==' . $remember_token . sha1($user_id . 'ratonlaveurs'), time() + 60 * 60 * 24 * 7);

    }

    /**
     * Permet de déconnecter l'utilisateur
     */		
    public function logout(){
        setcookie('remember', NULL, -1);
        $this->session->delete('auth');
    }

    /**
     * Permet de réinitialiser le mot de passe
     * @param $db	 
     * @param $email
     * @return l'utilisateur
     */		
    public function resetPassword($db, $email){
        $user = $db->query('SELECT * FROM UTILISATEURS WHERE EMAIL = ? AND CONFIRMER_DATE IS NOT NULL', [$email])->fetch();
        if($user){
            $reset_token = Str::random(60);
            $db->query('UPDATE UTILISATEURS SET REINIT_TOKEN = ?, REINIT_DATE = NOW() WHERE NUM_UTILISATEURS = ?', [$reset_token, $user->id]);
            mail($_POST['email'], 'Réinitiatilisation de votre mot de passe', "Afin de réinitialiser votre mot de passe merci de cliquer sur ce lien\n\nhttp://local.dev/Lab/Comptes/reset.php?id={$user->id}&token=$reset_token");
            return $user;
        }
        return false;
    }
	
    /**
     * Permet de vérifier si le token est valable
     * @param $db	 
     * @param $user_id
	 * @param $token
     * @return l'utilisateur
     */		
    public function checkResetToken($db, $user_id, $token){
        return $db->query('SELECT * FROM UTILISATEURS WHERE NUM_UTILISATEURS = ? AND REINIT_TOKEN IS NOT NULL AND REINIT_TOKEN = ? AND REINIT_DATE > DATE_SUB(NOW(), INTERVAL 30 MINUTE)', [$user_id, $token])->fetch();
    }

}