<?php
class Session{

    static $instance;

    /**
     * Permet d'initialiser l'instance sauvegarder
     * @return instance
     */		
    static function getInstance(){
        if(!self::$instance){
            self::$instance = new Session();
        }
        return self::$instance;
    }

	/**
     * Session constructor.  
     */		
    public function __construct(){
        session_start();
    }

    /**
     * Permet de définir un message flash
	 * @param $key
	 * @param $message
     */		
    public function setFlash($key, $message){
        $_SESSION['flash'][$key] = $message;
    }

    /**
     * Permet de savoir s'il y a des messages flash en mémoire
     * @return bool
     */		
    public function hasFlashes(){
        return isset($_SESSION['flash']);
    }

    /**
     * Permet de renvoyer tous les messages flash
     * @return flash
     */		
    public function getFlashes(){
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        return $flash;
    }

    /**
     * Permet d'écrire dans la session proposer
     * @param $key
     */	
    public function write($key, $value){
        $_SESSION[$key] = $value;
    }

    /**
     * Permet de lire la session proposer
     * @param $key
     */	
    public function read($key){
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

	
    /**
     * Permet de détruire la session proposer
     * @param $key
     */
    public function delete($key){
        unset($_SESSION[$key]);
    }

}