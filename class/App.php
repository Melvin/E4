﻿<?php
class App{

    static $db = null;
   
    /**
     * Permet de stocker des offres 
     * @return l'offre
     */		
	static function getOffers()
    {
        return [
            [
                'name' => 'Inscription',
                'price' => 10,
                'price_text' => '10€/ans',
                'period' => 'Year'
            ]
        ];
    }

    /**
     * Permet d'initialiser la connexion à la base de donnée
     * @return database
     */		
    static function getDatabase(){
        if(!self::$db){
            self::$db = new Database('dbo723051449', 'Psychologie9672@', 'db723051449');
        }
        return self::$db;
    }

    /**
     * Permet d'initialiser la classe auth
     * @return instance Auth
     */		
    static function getAuth(){
        return new Auth(Session::getInstance(), ['restriction_msg' => 'non, STOPPPPPPPPP']);
    }

    /**
     * Permet de ce rediriger vers une page demander
     * @param $page
     */		
    static function redirect($page){
        header("Location: $page");
        exit();
    }

}