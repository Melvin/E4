<?php

class PaypalSubscription
{

    private $username;
    private $password;
    private $signature;
    private $offers;
    private $endpoint;
    private $sandbox;

    /**
     * PaypalSubscription constructor.
     * @param $username string
     * @param $password string
     * @param $signature string
     * @param $offers array Liste les offres disponibles
     * @param bool $sandbox
     */
    public function __construct($username, $password, $signature, $offers, $sandbox = true)
    {
        $this->username = $username;
        $this->password = $password;
        $this->signature = $signature;
        $this->offers = $offers;
        $this->endpoint = "https://api-3t." . ($sandbox ? "sandbox." : "") . "paypal.com/nvp";
        $this->sandbox = $sandbox;
    }

    /**
     * Permet de faire une requête NVP auprès de l'api Paypal
     * @param array $options
     * @return array Réponse paypal
     */
    public function nvp($options = [])
    {
        $curl = curl_init();
        $data = [
            'USER' => $this->username,
            'PWD' => $this->password,
            'SIGNATURE' => $this->signature,
            'VERSION' => 86,
        ];
        $data = array_merge($data, $options);
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->endpoint,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_SSL_VERIFYPEER => false, // à activer si certif pourris
            CURLOPT_POSTFIELDS => http_build_query($data)
        ]);
        $response = curl_exec($curl);
        if ($response === false) {
            throw new Exception(curl_error($curl));
        }
        $responseArray = [];
        parse_str($response, $responseArray);
        return $responseArray;
    }

    /**
     * Permet de souscrire à une offre (redirige vers paypal)
     * @param $offer_id
     * @throws Exception
     */
    public function subscribe($offer_id)
    {
        if (!isset($this->offers[$offer_id])) {
            throw new Exception('Cette offre n\'existe pas');
        }
        $offer = $this->offers[$offer_id];
        $data = [
            'METHOD' => 'SetExpressCheckout',
            'PAYMENTREQUEST_0_AMT' => $offer['price'],
            'PAYMENTREQUEST_0_ITEMAMT' => $offer['price'],
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'EUR',
            'PAYMENTREQUEST_0_CUSTOM' => $offer_id,
            'L_BILLINGTYPE0' => 'RecurringPayments',
            'L_BILLINGAGREEMENTDESCRIPTION0' => $offer['name'],
            'cancelUrl' => 'https://www.japan-zone.fr/Soutient',
            'returnUrl' => 'https://www.japan-zone.fr/process.php'
        ];
        $response = $this->nvp($data);
        if (!isset($response['TOKEN'])) {
            throw new Exception($response['L_LONGMESSAGE0']);
        }
        $token = $response['TOKEN'];
        $url = "https://www." . ($this->sandbox ? "sandbox." : "") . "paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=$token";
        header('Location: ' . $url);
    }

    /**
     * Permet d'obtenir les informations concernant la transaction en cours
     * @param $token
     * @return array
     */
    public function getCheckoutDetail($token)
    {
        $data = [
            'METHOD' => 'GetExpressCheckoutDetails',
            'TOKEN' => $token
        ];
        return $this->nvp($data);
    }

    /**
     * Crée un profile côté paypal et sauvegarde les informations au niveau de la base de données
     * @param $token
     * @param $user_id
     * @param $db
     * @throws Exception
     */
    public function doSubscribe($token, $db)
    {
        $detail = $this->getCheckoutDetail($token);
        $offer_id = $detail['PAYMENTREQUEST_0_CUSTOM'];
        if (!isset($this->offers[$offer_id])) {
            throw new Exception('Cette offre n\'existe pas');
        }
        $offer = $this->offers[$offer_id];
        $period = $offer['period'] === 'Month' ? new DateInterval('P1M') : new DateInterval('P1Y');
        $start = (new DateTime())->add($period)->getTimestamp();
        $response = $this->nvp([
            'METHOD' => 'CreateRecurringPaymentsProfile',
            'TOKEN' => $token,
            'PAYERID' => $detail['PAYERID'],
            'DESC' => $offer['name'],
            'AMT' => $offer['price'],
            'BILLINGPERIOD' => $offer['period'],
            'BILLINGFREQUENCY' => 1,
            'CURRENCYCODE' => 'EUR',
            'COUNTRYCODE' => 'FR',
            'MAXFAILEDPAYMENTS' => 3,
            'PROFILESTARTDATE' => gmdate("Y-m-d\TH:i:s\Z", $start),
            'INITAMT' => $offer['price']
        ]);
        if ($response['ACK'] === 'Success') {
			$db->query("INSERT INTO COMMANDES SET PAYER_ID = ?, PROFILE_ID = ?", [
                $detail['PAYERID'],
                $response['PROFILEID'],
            ]);
        } else {
            throw new Exception($response['L_LONGMESSAGE0']);
        }
    }

    /**
     * Obtient les détails concernant un profil d'abonnement
     * @param $profile_id
     * @return array
     */
    public function getProfileDetail($profile_id)
    {
        return $this->nvp([
            'METHOD' => 'GetRecurringPaymentsProfileDetails',
            'PROFILEID' => $profile_id
        ]);
    }

    /**
     * Vérifie l'authenticité d'une notification instantanée de paiement
     * @param $data
     * @return bool
     */
    public function verifyIPN($data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://www." . ($this->sandbox ? "sandbox." : "") . "paypal.com/cgi-bin/webscr?cmd=_notify-validate&" . http_build_query($data),
            CURLOPT_RETURNTRANSFER => 1,
            // CURLOPT_SSL_VERIFYPEER => false // à activer si certif pourris
        ]);
        $response = curl_exec($curl);
        return $response === 'VERIFIED';
    }

    /**
     * Annule le profil d'abonnement
     * @param $profile_id
     * @return bool
     * @throws Exception
     */
    public function unsubscribe($profile_id)
    {
        $response = $this->nvp([
            'METHOD' => 'ManageRecurringPaymentsProfileStatus',
            'PROFILEID' => $profile_id,
            'ACTION' => 'Cancel'
        ]);
        if ($response['ACK'] === 'Success') {
            return true;
        }
        throw new Exception($response['L_LONGMESSAGE0']);
    }

}
?>