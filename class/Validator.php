<?php

class Validator
{

    private $data;
    private $errors = [];

	/**
     * Validator constructor.  
     * @param $data string	  
     */		
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Permet de vérifier si le champ existe
     * @param $field
	 * @return field
     */	
    private function getField($field)
    {
        if (!isset($this->data[$field])) {
            return null;
        }
        return $this->data[$field];
    }

    /**
     * Permet de vérifier si le paramétre est alphanumérique
     * @param $field
	 * @param $errorMsg
	 * @return bool
     */		
    public function isAlpha($field, $errorMsg)
    {
        if (!preg_match('/^[a-zA-Z0-9_]+$/', $this->getField($field))) {
            $this->errors[$field] = $errorMsg;
            return false;
        }
        return true;
    }

    /**
     * Permet de vérifier si le paramétre existe déjà dans la base de donnée
     * @param $field
	 * @param $db
	 * @param $table
	 * @param $errorMsg
	 * @return bool
     */		
    public function isUniq($field, $db, $table, $errorMsg)
    {
        $record = $db->query("SELECT NUM_UTILISATEURS FROM $table WHERE $field = ?", [$this->getField($field)])->fetch();
        if ($record) {
            $this->errors[$field] = $errorMsg;
            return false;
        }
        return true;
    }

    /**
     * Permet de vérifier si le paramétre est bien un email
     * @param $field
	 * @param $errorMsg
	 * @return bool
     */		
    public function isEmail($field, $errorMsg)
    {
        if (!filter_var($this->getField($field), FILTER_VALIDATE_EMAIL)) {
            $this->errors[$field] = $errorMsg;
            return false;
        }
        return true;
    }

    /**
     * Permet de vérifier et de confirmer le mot de passe saisie
     * @param $field
	 * @param $errorMsg
	 * @return bool
     */		
    public function isConfirmed($field, $errorMsg = '')
    {
        $value = $this->getField($field);
        if (empty($value) || $value != $this->getField($field . '_confirm')) {
            $this->errors[$field] = $errorMsg;
            return false;
        }
        return true;
    }

    /**
     * Permet de vérifier si les données sont valide
     * @return bool
     */		
    public function isValid()
    {
        return empty($this->errors);
    }

    /**
     * Permet de stocker les erreurs en extérieur à fin de les affichers
     * @return array errors
     */		
    public function getErrors()
    {
        return $this->errors;
    }

}