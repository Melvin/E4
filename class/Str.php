<?php
class Str{

    /**
     * Permet de mélanger une chaîne de caratère de 'n' caractère 
	 * @param $length
     * @return string
     */	
    static function random($length){
        $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
        return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
    }

}